﻿using PottencialAPI.Domain;
using PottencialAPI.Infra.Data;

namespace PottencialAPI.Endpoints.Vendas
{
    public class VendaPost
    {
        public static string Template => "/venda";
        public static string[] Methods => new string[] { HttpMethod.Post.ToString() };
        public static Delegate Handle => Action;
        public static IResult Action(VendaRequest vendaRequest, AppDbContext context)
        {
            var vendedor = vendaRequest.VendedorId; 
            var produtos = context.Produtos.Where(p => vendaRequest.ProdutosId.Contains(p.Id)).ToList();

            var venda = new Venda(vendedor, produtos);
            context.Vendas.Add(venda);
            context.SaveChanges();

            return Results.Created($"/venda/{venda.Id}", venda.Id);
        }
    }
}
