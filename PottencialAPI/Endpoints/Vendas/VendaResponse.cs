﻿using PottencialAPI.Domain;
using System.ComponentModel.DataAnnotations;
using System.Xml.Linq;

namespace PottencialAPI.Endpoints.Vendas
{
    public record VendaResponse
    {
        public Guid Id { get; set; }
        public List<Produto> Produtos { get; set; }
        public int Status { get; set; }
        public DateTime Data { get; set; }
        IEnumerable<ProdutoVenda> ProdutoVenda { get; set; }
    }

    public record ProdutoVenda(Guid Id, string Descricao);
}