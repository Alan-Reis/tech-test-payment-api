﻿using PottencialAPI.Domain;

namespace PottencialAPI.Endpoints.Vendas
{
    public record VendaRequest(Guid VendedorId, List<Guid> ProdutosId);
}