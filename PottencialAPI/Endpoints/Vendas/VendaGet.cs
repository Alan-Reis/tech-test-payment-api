﻿using Microsoft.EntityFrameworkCore;
using PottencialAPI.Endpoints.Vendas;
using PottencialAPI.Infra.Data;

namespace PottencialAPI.Endpoints.Vendas
{
    public class VendaGet
    {
        public static string Template => "/venda/{id}";
        public static string[] Methods => new string[] { HttpMethod.Get.ToString() };
        public static Delegate Handle => Action;
        public static IResult Action(Guid id, AppDbContext context)
        {
            var vendas = context.Vendas.Include(o => o.Produtos).FirstOrDefault(o => o.Id == id);
            var vendedor = context.Vendedores.Find(vendas.VendedorId);
            var produtos = vendas.Produtos.Select(p => new ProdutoVenda(p.Id, p.Descricao));
            var data = vendas.Data.ToString("dd/MM/yyy HH:mm");

            var status = vendas.StatusDeVenda(vendas.Status);


            var response = new
            {
                vendas.Id,
                status,
                data,
                vendas.VendedorId,
                vendedor.Nome,
                vendedor.CPF,
                vendedor.Telefone,
                vendedor.Email,
                produtos
                
            };

            return Results.Ok(response);
        }
    }
}
