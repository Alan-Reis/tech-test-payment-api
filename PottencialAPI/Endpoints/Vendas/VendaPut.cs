﻿using Microsoft.AspNetCore.Mvc;
using PottencialAPI.Infra.Data;

namespace PottencialAPI.Endpoints.Vendas
{
    public class VendaPut
    {
        public static string Template => "/venda/{id}";
        public static string[] Methods => new string[] { HttpMethod.Put.ToString() };
        public static Delegate Handle => Action;
        public static IResult Action([FromRoute] Guid id, VendaRequestPut vendaRequest, AppDbContext context)
        {
            var venda = context.Vendas.Where(c => c.Id == id).FirstOrDefault();

            int statusOld = venda.Status;
            int statusNew = vendaRequest.Status;

            var mensagem = venda.VerificaStatus(statusOld, statusNew);

            if(mensagem == "Sucesso")
            {
                venda.Status = vendaRequest.Status;
                context.SaveChanges();
            }

            return Results.Ok(mensagem);
        }
    }
}
