﻿
namespace PottencialAPI.Endpoints.Vendas
{
    /// <summary>
    /// <exemple>Definição dos Status: </exemple>
    /// <exemple>Status 0 = Aguardando pagamento | </exemple>
    /// <exemple>Status 1 = Pagamento aprovado | </exemple>
    /// <exemple>Status 2 = Enviado para transportadora | </exemple>
    /// <exemple>Status 3 = Entregue | </exemple>
    /// <exemple>Status 4 = Cancelada</exemple>
    /// </summary>
    /// <param name="Status"></param>
    public record VendaRequestPut(int Status);
}

