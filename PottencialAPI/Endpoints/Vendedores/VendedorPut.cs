using Microsoft.AspNetCore.Mvc;
using PottencialAPI.Infra.Data;

namespace PottencialAPI.Endpoints.Vendedores
{
    public class VendedorPut
    {
        public static string Template => "/vendedor/{id}";
        public static string[] Methods => new string[] { HttpMethod.Put.ToString() };
        public static Delegate Handle => Action;
        public static IResult Action([FromRoute] Guid id, VendedorRequest vendedorRequest, AppDbContext context)
        {
            var vendedor = context.Vendedores.Where(c => c.Id == id).FirstOrDefault();

            vendedor.Telefone = vendedorRequest.Telefone;
            vendedor.Email = vendedorRequest.Email;
            context.SaveChanges();

            return Results.Ok();
        }
    }
}