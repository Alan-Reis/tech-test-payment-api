﻿namespace PottencialAPI.Endpoints.Vendedores
{
    internal class VendedorResponse
    {
        public Guid Id { get; set; }
        public string Nome { get; set; }
        public string CPF { get; set; }
        public string Email { get; set; }
        public string Telefone { get; set; }
    }
}