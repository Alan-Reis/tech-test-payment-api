using PottencialAPI.Domain;
using PottencialAPI.Infra.Data;

namespace PottencialAPI.Endpoints.Vendedores
{
    public class VendedorPost
    {
        public static string Template => "/vendedor";
        public static string[] Methods => new string[] { HttpMethod.Post.ToString() };
        public static Delegate Handle => Action;
        public static IResult Action(VendedorRequest vendedorRequest, AppDbContext context)
        {
            var vendedor = new Vendedor
            {
                Nome = vendedorRequest.Nome,
                CPF = vendedorRequest.CPF,
                Telefone = vendedorRequest.Telefone,
                Email = vendedorRequest.Email
            };
            context.Vendedores.Add(vendedor);
            context.SaveChanges();

            return Results.Created($"/vendedores/{vendedor.Id}", vendedor.Id);
        }
    }
}