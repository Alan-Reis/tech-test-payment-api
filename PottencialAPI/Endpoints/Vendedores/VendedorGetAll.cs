using PottencialAPI.Infra.Data;

namespace PottencialAPI.Endpoints.Vendedores
{
    public class VendedorGetAll
    {
        public static string Template => "/vendedor";
        public static string[] Methods => new string[] { HttpMethod.Get.ToString() };
        public static Delegate Handle => Action;
        public static IResult Action(AppDbContext context)
        {
            var vendedores = context.Vendedores.ToList();

            var response = vendedores.Select(f => new VendedorResponse { Id = f.Id, Nome = f.Nome, CPF = f.CPF, Telefone = f.Telefone, Email = f.Email });

            return Results.Ok(response);
        }
    }
}