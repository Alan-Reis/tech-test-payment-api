using PottencialAPI.Domain;
using PottencialAPI.Infra.Data;

namespace PottencialAPI.Endpoints.Produtos
{
    public class ProdutoPost
    {
        public static string Template => "/produto";
        public static string[] Methods => new string[] { HttpMethod.Post.ToString() };
        public static Delegate Handle => Action;
        public static IResult Action(ProdutoRequest produtoRequest, AppDbContext context)
        {
            var produto = new Produto
            {
                Descricao = produtoRequest.Descricao
            };
            context.Produtos.Add(produto);
            context.SaveChanges();

            return Results.Created($"/produtos/{produto.Id}", produto.Id);
        }
    }
}