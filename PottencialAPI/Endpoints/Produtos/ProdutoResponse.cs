﻿using PottencialAPI.Domain;

namespace PottencialAPI.Endpoints.Produtos
{
    internal class ProdutoResponse
    {
        public Guid Id { get; set; }
        public string Descricao { get; set; }
    }
}