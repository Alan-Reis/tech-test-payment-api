using PottencialAPI.Infra.Data;


namespace PottencialAPI.Endpoints.Produtos
{
    public class ProdutoGetAll
    {
        public static string Template => "/produto";
        public static string[] Methods => new string[] { HttpMethod.Get.ToString() };
        public static Delegate Handle => Action;
        public static IResult Action(AppDbContext context)
        {
            var produtos = context.Produtos.ToList();

            var response = produtos.Select(p => new ProdutoResponse { Id = p.Id, Descricao = p.Descricao });

            return Results.Ok(response);
        }
    }
}