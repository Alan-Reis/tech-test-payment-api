using Microsoft.AspNetCore.Mvc;
using PottencialAPI.Infra.Data;

namespace PottencialAPI.Endpoints.Produtos
{
    public class ProdutoPut
    {
        public static string Template => "/produto/{id}";
        public static string[] Methods => new string[] { HttpMethod.Put.ToString() };
        public static Delegate Handle => Action;
        public static IResult Action([FromRoute] Guid id, ProdutoRequest produtoRequest, AppDbContext context)
        {
            var produto = context.Produtos.Where(c => c.Id == id).FirstOrDefault();

            produto.Descricao = produtoRequest.Descricao;
            context.SaveChanges();

            return Results.Ok();
        }
    }
}