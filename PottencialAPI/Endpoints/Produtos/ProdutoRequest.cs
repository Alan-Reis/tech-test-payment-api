﻿using PottencialAPI.Domain;

namespace PottencialAPI.Endpoints.Produtos
{ 
    public class ProdutoRequest
    {
        public Guid Id { get; set; }
        public string Descricao { get; set; }
    }
}