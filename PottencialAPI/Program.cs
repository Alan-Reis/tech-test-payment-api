using Microsoft.OpenApi.Models;
using PottencialAPI.Endpoints.Produtos;
using PottencialAPI.Endpoints.Vendas;
using PottencialAPI.Endpoints.Vendedores;
using PottencialAPI.Infra.Data;
using System.Reflection;
using ProdutoGetAll = PottencialAPI.Endpoints.Produtos.ProdutoGetAll;

var builder = WebApplication.CreateBuilder(args);

builder.Services.AddSqlServer<AppDbContext>(builder.Configuration["ConnectionStrings:ConexaoPadrao"]);

builder.Services.AddControllers();
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen(options =>
{
    options.SwaggerDoc("v1", new OpenApiInfo
    {
        Version = "v1",
        Title = "tech-test-payment-api",
        Description = "API Desenvolvida para Pottencial Seguradora como teste do bootcamp na DIO."
    });

    var xmlFile = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
    var xmlPath = Path.Combine(AppContext.BaseDirectory, xmlFile);
    options.IncludeXmlComments(xmlPath);
});

var app = builder.Build();

if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI(c =>
    {   
        c.SwaggerEndpoint("/swagger/v1/swagger.json","V1 Docs");
    }); 
}

app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();

app.MapMethods(ProdutoPost.Template, ProdutoPost.Methods, ProdutoPost.Handle);
app.MapMethods(ProdutoGetAll.Template, ProdutoGetAll.Methods, ProdutoGetAll.Handle);
app.MapMethods(ProdutoPut.Template, ProdutoPut.Methods, ProdutoPut.Handle);

app.MapMethods(VendedorPost.Template, VendedorPost.Methods, VendedorPost.Handle);
app.MapMethods(VendedorGetAll.Template, VendedorGetAll.Methods, VendedorGetAll.Handle);
app.MapMethods(VendedorPut.Template, VendedorPut.Methods, VendedorPut.Handle);

app.MapMethods(VendaPost.Template, VendaPost.Methods, VendaPost.Handle);
app.MapMethods(VendaGet.Template, VendaGet.Methods, VendaGet.Handle);
app.MapMethods(VendaPut.Template, VendaPut.Methods, VendaPut.Handle);


app.Run();
