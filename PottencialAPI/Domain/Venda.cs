﻿namespace PottencialAPI.Domain
{
    public class Venda 
    {
        public Guid Id { get; set; }
        public Guid VendedorId { get; set; }
        public List<Produto> Produtos { get; set; }
        public int Status { get; set; }
        public DateTime Data { get; set; }

        public Venda()
        {

        }

        public Venda(Guid vendedorId, List<Produto> produtos)
        {
            Status = 0;
            VendedorId = vendedorId;
            Data = DateTime.Now;
            Produtos = produtos;
        }

        public string StatusDeVenda(int statusVenda)
        {
            var status = "";

            switch (statusVenda)
            {
                case 1:
                    status = "Pagamento aprovado";
                    break;
                case 2:
                    status = "Enviado para transportadora";
                    break;
                case 3:
                    status = "Entregue";
                    break;
                case 4:
                    status = "Cancelada";
                    break;
                default: status = "Aguardando pagamento";
                    break;
            }
            return status;
        }

        public string VerificaStatus(int statusOld, int statusNew)
        {
            var mensagem = "Tentativa de atualizar o Status de venda falhou!";

            switch (statusOld)
            {
                case 0:
                    mensagem = ValidaUpdateAguardandoPagamento(statusNew);
                    break;
                case 1:
                    mensagem = ValidaUpdatePagamentoAprovado(statusNew);
                    break;
                case 2:
                    mensagem = ValidaUpdateEnviadoTransportadora(statusNew);
                    break;
                case 3:
                    mensagem = "Produto entregue!";
                    break;
            }
            return mensagem;
        }

        public string ValidaUpdateAguardandoPagamento(int status)
        {
            if (status is 1 or 4)
            {
                return ("Sucesso");
            }
            return ("A venda só pode ser atualizada para 1 - Pagamento aprovado ou 4 - Cancelada");
        }

        public string ValidaUpdatePagamentoAprovado(int status)
        {
                if (status is 2 or 4)
                {
                    return ("Sucesso");
                }
                return ("A venda só pode ser atualizada para 2 - Enviado para transportadora ou 4 - Cancelada");
        }

        public string ValidaUpdateEnviadoTransportadora(int status)
        {
            if (status is 3)
            {
                return ("Sucesso");
            }
            return ("A venda só pode ser atualizada para 3 - Entregue");
        }
    }
}
