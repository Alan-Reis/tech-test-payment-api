namespace PottencialAPI.Domain
{
    public class Produto
    {
        public Guid Id { get; set; }
        public string Descricao { get; set; }
        public ICollection<Venda> Vendas { get; set; }
    }
}