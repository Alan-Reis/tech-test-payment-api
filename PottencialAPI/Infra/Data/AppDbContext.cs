﻿using PottencialAPI.Domain;
using Microsoft.EntityFrameworkCore;

namespace PottencialAPI.Infra.Data
{
    public class AppDbContext : DbContext
    {
        public DbSet<Produto> Produtos { get; set; }
        public DbSet<Vendedor> Vendedores { get; set; }
        public DbSet<Venda> Vendas { get; set; }

        public AppDbContext(DbContextOptions<AppDbContext> options) : base(options)
        {

        }

        protected override void OnModelCreating(ModelBuilder builder)
        {

            builder.Entity<Produto>()
                .Property(p => p.Descricao).HasMaxLength(255).IsRequired(false);

            builder.Entity<Vendedor>()
                .Property(f => f.Nome).IsRequired();

            builder.Entity<Vendedor>()
                .Property(f => f.CPF).HasMaxLength(20).IsRequired();

            builder.Entity<Vendedor>()
                .Property(f => f.Telefone).HasMaxLength(20).IsRequired();

            builder.Entity<Venda>()
                .Property(v => v.VendedorId).IsRequired();

            builder.Entity<Venda>()
                .HasMany(p => p.Produtos)
                .WithMany(v => v.Vendas)
                .UsingEntity(x => x.ToTable("ProdutoVenda"));

        }

        protected override void ConfigureConventions(ModelConfigurationBuilder configuration)
        {
            configuration.Properties<string>()
                .HaveMaxLength(100);
        }
    }
}
